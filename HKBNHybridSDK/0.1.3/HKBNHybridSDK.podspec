#
# Be sure to run `pod lib lint HKBNHybridSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HKBNHybridSDK'
  s.version          = '0.1.3'
  s.summary          = 'Test of HKBNHybridSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/keassze/testhybridsdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Steve.cc.ho' => 'steve.cc.ho@hkbn.com.hk' }
  s.source           = { :git => 'https://gitlab.com/keassze/testhybridsdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.ios.deployment_target = '11.0'

#  s.source_files = 'HKBNHybridSDK/Classes/{HKBNHybridSDK.h,Config/*}'

  s.source_files = 'HKBNHybridSDK/Classes/HKBNHybridSDK.{h,m}'
  s.resource_bundles = {
     'HKBNHybridSDKBundle' => ['HKBNHybridSDK/Assets/*.{json,png}']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  
  s.frameworks = 'UIKit', 'Foundation', 'Security'
  s.ios.libraries = 'c++','z'
  
#  s.dependency 'AFNetworking',  '~> 4.0.0'
#  s.dependency 'SSZipArchive',  '~> 2.4.3'
#  s.dependency 'MBProgressHUD'
  
  #s.dependency 'KKLog'
  
  s.pod_target_xcconfig = {
  'GENERATE_INFOPLIST_FILE' => 'YES',
  'CODE_SIGNING_ALLOWED' => 'NO',
  'CODE_SIGNING_REQUIRED'=> 'NO',
  # c++ code
  'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
  'CLANG_CXX_LIBRARY' => 'libc++',
  'ENABLE_BITCODE' => 'NO'}
  #s.user_target_xcconfig = {
    #'GENERATE_INFOPLIST_FILE' => 'YES',
    #'CODE_SIGNING_ALLOWED' => 'NO',
    #'CODE_SIGNING_REQUIRED'=> 'NO',
    #'EXPANDED_CODE_SIGN_IDENTITY' => '',
    #}
  #s.info_plist = {'CFBundleIdentifier' => 'org.cocoapods.HKBNHybridSDK',}
  
  
  s.subspec 'Utils' do |ss|
      ss.source_files = 'HKBNHybridSDK/Classes/Utils/**/*'
      ss.dependency 'HKBNHybridSDK/ThirdPart'
      ss.dependency 'AFNetworking',  '~> 4.0.0'
      ss.dependency 'HXPhotoPicker', '~> 3.3.2'
      ss.dependency 'YYCache',       '~> 1.0.4'
  end
  s.subspec 'Core' do |ss|
      ss.source_files = 'HKBNHybridSDK/Classes/Core/**/*'
      ss.dependency 'HKBNHybridSDK/Utils'
      ss.dependency 'HKBNHybridSDK/Route'
  end
  s.subspec 'Containner' do |ss|
      ss.source_files = 'HKBNHybridSDK/Classes/Containner/**/*'
      ss.dependency 'HKBNHybridSDK/Core'
      ss.dependency 'HKBNHybridSDK/UI'
      ss.dependency 'MJRefresh'
      ss.dependency 'GKNavigationBar'
  end
  s.subspec 'Route' do |ss|
      ss.source_files = 'HKBNHybridSDK/Classes/Route/**/*'
      ss.dependency 'HKBNHybridSDK/Utils'
      ss.dependency 'SSZipArchive',  '~> 2.4.3'
  end
  s.subspec 'ThirdPart' do |ss|
      ss.source_files = 'HKBNHybridSDK/Classes/ThirdPart/**/*'
  end
  s.subspec 'UI' do |ss|
      ss.source_files = 'HKBNHybridSDK/Classes/UI/*.{h,m}'
  end
  
end
