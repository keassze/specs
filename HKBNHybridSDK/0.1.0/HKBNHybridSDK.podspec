#
# Be sure to run `pod lib lint HKBNHybridSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HKBNHybridSDK'
  s.version          = '0.1.0'
  s.summary          = 'Test of HKBNHybridSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/keassze/testhybridsdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Steve.cc.ho' => 'steve.cc.ho@hkbn.com.hk' }
  s.source           = { :git => 'https://gitlab.com/keassze/testhybridsdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.ios.deployment_target = '11.0'

  s.source_files = 'HKBNHybridSDK/Classes/**/*'
  
  # s.resource_bundles = {
  #   'HKBNHybridSDK' => ['HKBNHybridSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  
  s.frameworks = 'UIKit', 'Foundation', 'Security'
  s.ios.libraries = 'c++','z'
  s.dependency 'AFNetworking',  '~> 4.0.0'
  s.dependency 'SSZipArchive',  '~> 2.4.3'
  s.dependency 'MJRefresh'
  s.dependency 'MBProgressHUD'
  
  s.dependency 'GKNavigationBar'
  s.dependency 'HXPhotoPicker', '~> 3.3.2'
  s.dependency 'YYCache',       '~> 1.0.4'
  #s.dependency 'KKLog'
  s.pod_target_xcconfig = {
  'GENERATE_INFOPLIST_FILE' => 'YES',
  'CODE_SIGNING_ALLOWED' => 'NO',
  'CODE_SIGNING_REQUIRED'=> 'NO',
  # c++ code
  'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
  'CLANG_CXX_LIBRARY' => 'libc++',
  'ENABLE_BITCODE' => 'NO'}
  #s.user_target_xcconfig = {
    #'GENERATE_INFOPLIST_FILE' => 'YES',
    #'CODE_SIGNING_ALLOWED' => 'NO',
    #'CODE_SIGNING_REQUIRED'=> 'NO',
    #'EXPANDED_CODE_SIGN_IDENTITY' => '',
    #}
  #s.info_plist = {'CFBundleIdentifier' => 'org.cocoapods.HKBNHybridSDK',}
  
end
